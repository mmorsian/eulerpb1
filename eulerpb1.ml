(* 
This program gives a generalized solution to the problem:

"Find the sum of all the multiples of x or y below n."
*)


(* this function returns the sum of the multiples of a below b
   it is really a wrapper for a recursive function  which needs an extra 
   "mul" parameter to keep track of the latest multiple *)

let sum_multiple a b = 

  let rec sum_multiple_rec mul = 
    match (mul >= b) with 
    | true -> 0 
    | false -> mul + sum_multiple_rec (a + mul) in

    sum_multiple_rec 0;;



(* Main program. Takes the three arguments on the command line.
   Print usage instructions if invoked without arguments or with wrong arguments.  *)

let main () =

  try

    let x = int_of_string Sys.argv.(1) 
    and y = int_of_string Sys.argv.(2) 
    and n = int_of_string Sys.argv.(3) in 
      print_int (sum_multiple x n + sum_multiple y n); print_newline ()

  with _ -> Printf.printf "usage : %s x y n\n" Sys.argv.(0) ;;

main ();;
